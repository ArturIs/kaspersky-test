#include <fstream>
#include <filesystem>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include "utility.h"


// функкция, реализующая проверку директории на налчие подозрительных файлов
void DirectoryScanner::inspect_file(const std::string &file_path)
{
    std::ifstream file(file_path);
    std::mutex m;
    if (!file)
    {
        errors_count++;
        return;
    }


    std::string  str;
    while (file)
    {
        std::getline(file, str);
        if (str == "<script>evil_script()</script>")
        {
            std::lock_guard<std::mutex> mtx(m);
            js_count++;
            return;
        }
        if (str == "rm -rf ~/Documents")
        {
            std::lock_guard<std::mutex> mtx(m);
            unix_count++;
            return;;
        }
        if (str == "system(\"launchctl load /Library/LaunchAgents/com.malware.agent\")")
        {
            std::lock_guard<std::mutex> mtx(m);
            macos_count++;
            return;
        }
    }
}



std::string DirectoryScanner::inspect_directory()
{
    std::queue<std::string> files;
    std::vector<std::thread> threads;
    std::mutex m;
    const unsigned short threads_num = std::thread::hardware_concurrency();

    try
    {
        for (const auto & file : std::filesystem::directory_iterator(dir_path))
            files.push(file.path());
    }
    catch (std::filesystem::filesystem_error &err)
    {
        return "Directory reading error\n";
    }

    // Распараллеливание сканирования различных файлов по потокам
    for (int i = 0; i < threads_num; ++i) {
        std::thread t( [&files, &m, this](int num) {
            while (true)
            {
                m.lock();
                if ( !files.empty() ) {
                    std::string file = files.front();
                    files_num++;
                    files.pop();
                    m.unlock();
                    inspect_file(file);
                } else
                {
                    m.unlock();
                    break;
                }
            }
        }, i);
        threads.push_back(std::move(t));
    }
    for( std::thread& t : threads ) {
        t.join();
    }

    // строка с результатом работы
    std::string log_string = "Processed files: " + std ::to_string(files_num) + '\n';
    log_string += "js count: " + std::to_string(js_count) + '\n';
    log_string += "unix count: " + std::to_string(unix_count) + '\n';
    log_string += "macos count: " + std::to_string(macos_count) + '\n';
    log_string += "errors count: " + std::to_string(errors_count) + '\n';
    js_count = 0; unix_count = 0; macos_count = 0; errors_count = 0;  files_num = 0;
    return log_string;
}


