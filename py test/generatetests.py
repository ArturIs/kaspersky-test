import random
import string
import os

def generate_random_string(length):
    letters = string.ascii_lowercase
    rand_string = ''.join(random.choice(letters) for i in range(length))
    return rand_string + '\n'

threats = {
            'js': '<script>evil_script()</script>',
            'unix': 'rm -rf ~/Documents',
            'macos': 'system("launchctl load /Library/LaunchAgents/com.malware.agent")'
          }
threat_counts = {'js': 0, 'unix': 0, 'macos': 0, 'error' : 0}

for file in os.scandir("/home/artur/projects/C++/KasperskyTest/test"):
    os.remove(file)

for i in range(1, 101):
    filename  = f"/home/artur/projects/C++/KasperskyTest/test/{i}"
    with open(filename, 'w') as f:
        str_num, threat_pos = random.randint(10, 50), -1
        error_flag = random.random() < 0.05
        if random.random() > 0.75: # записывать ли подозрительную строку
            threat_pos = random.randint(1, str_num)
            threat_type = random.choice(['js', 'unix', 'macos'])
            threat_counts[threat_type] += 1 - error_flag
        for j in range(1, str_num + 1):
            if threat_pos == j:
                f.write(threats[threat_type] + '\n')
            else:
                f.write(generate_random_string(random.randint(10, 30)))
        if error_flag : # деать ли файл нечитаемым
            os.chmod(filename, 000)
            threat_counts['error'] += 1
print(threat_counts)

