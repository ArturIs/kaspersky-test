#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "utility.h"


const int BUFFER_SIZE = 512;               // размер буфера для обмена текстовыми сообщениями между клиентом и сервером
const int DEFAULT_PORT = 1234;             // порт по умолчания, на котором работает сервер
const int CLIENTS_MAX_NUM = 5;             // ограничение на число клиентов, с которыми может работать сервер
const int TIMEOUT = 120;                   // после 120 секунд без запросов сервер завершит работу
const std::string STOP_COMMAND = "stop";   // cтоп-слово для завершения соединения с клиентом


// Вспомогательная функция, выводящая ошибки для откладки
void error(const std::string &error_message) {
    std::cerr << error_message << '\n';
    exit(EXIT_FAILURE);
}



//*********************************************************************************************************************
// Часть программы, в которой инициализуется сервер.
int main(int argc, char *argv[]) {
    int listen_socket, port_num{DEFAULT_PORT};

    // Если в качестве аргумента коммандной строки был передан номер порта,
    // то программа попытается запустить сервер на нем.
    if (argc == 2) {
        try {
            port_num = std::stoi(argv[1]);
        }
        catch (std::invalid_argument) {
            error("incorrect port number provided");
        }
        std::cout << "The port provided is " << argv[1] << '\n';
    }

    //создание сокета listen_socket сервера, который будет принимать запросы на установления соединиения от клиентов
    listen_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_socket < 0)
        error("ERROR opening listener socket");

    // заполнение адресных данных для сервера.
    struct sockaddr_in server_addr{0};
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(port_num);

    // Настройка сокета на подключения от множества клиентов
    int opt = 1;
    if (setsockopt(listen_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) < 0)
        error("ERROR configuring a server listen-socket for multiple client connections");
    // Выделение порта под listen_socket
    if (bind(listen_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
        error("ERROR binding listen socket on port "  + std::to_string(port_num));
    // Проверка, готоч ли сокет принимать запросы от клиентов
    if (listen(listen_socket, 5) < 0)
        error("ERROR in listen(...) function");
    std::cout << "== Scan service has started on port " << port_num << " ==\n";



//**********************************************************************************************************************
// Часть программы, реализующая основной цикл обработки запросов от клиентов
    int client_sockets[CLIENTS_MAX_NUM] {0};
    char buffer[BUFFER_SIZE];
    struct sockaddr_in address;
    socklen_t  addrlen;
    fd_set sockets_set;


    while (true)
    {
        // На каждой итерации в начале заполняется множество sockets_set  файловых дескрипторов для сокетов,
        // обеспечивающих соединение с клинетами, которые не заершили сессию работы с сервером.

        // с момента последнего прохода часть клиентов могла прервать соединение с сервером,
        // поэтому sockets_set обноляется в начале итерации
        FD_ZERO(&sockets_set);

        //add master socket to set
        FD_SET(listen_socket, &sockets_set);
        int max_sd = listen_socket;

        // clients_sockets хранит файлоые дескрипторы для сокетов, соединяюих сервер с активными клиенатми
        // sd - socket descriptor
        for (const int &sd : client_sockets)
        {
            // sd - если корректный, то добавляем в множество  соединений, которые будут проверяться на новые сообщения
            if (sd > 0)
                FD_SET(sd, &sockets_set);

            //наибольший номер файлового дескриптора в socket_set, нужен для функции select
            if (sd > max_sd)
                max_sd = sd;
        }

        // функция select обновит описатели сокетов, которые отслеживающий изменения состояния соедниний
        // так, появление новых сообщений будет отслежено
        struct timeval tv = {TIMEOUT, 0};
        int select_result = select(max_sd + 1, &sockets_set, nullptr, nullptr, &tv);
        if (select_result < 0)
            error("ERROR selecting client socket");
        // если сервер будет простаивать время, большее заданного TIMEOUT, то он завершит работу
        if (select_result == 0)
        {
            std:: cout << "\nSERVER IS TIMED OUT AFTER " << TIMEOUT
                       << " SECOND INACTIVITY AND GOING TO COMPLETE ITS WORK\n";
            memset(buffer, 0, BUFFER_SIZE);
            break;
        }

        // Проверка, нет ли подключений от новых клиентов
        if (FD_ISSET(listen_socket, &sockets_set))
        {
            int new_socket;
            if ((new_socket = accept(listen_socket, (struct sockaddr *) &address, &addrlen)) < 0)
            {
                std::string  error_message;
                error_message += "ERROR accepting client - ip"  + std::string(inet_ntoa(address.sin_addr));
                error_message += ", port " + std::to_string(ntohs(address.sin_port)) + '\n';
                error(error_message);
            }
            std::cout << "New connection, socket fd is " << std::to_string(new_socket)
                      << " ip is " << inet_ntoa(address.sin_addr) << " port is " << ntohs(address.sin_port);

            // Дескриптор нового соединения будет записан в первую свободную ячейку массива, хранящего
            // файловые дескрипторы сокетов для соединений с клиентами, имеющими активную сессию работы с сервером
            for (int i = 0; i < CLIENTS_MAX_NUM; i++)
            {
                if (client_sockets[i] == 0)
                {
                    client_sockets[i] = new_socket;
                    std::cout << " (added to list of sockets as " << i <<")\n";
                    break;
                }
            }
        }


        // Все сущестующие активные соедниения с серером будут проверены на изменения состояния
        for (int &sd : client_sockets)
        {

            if (FD_ISSET(sd, &sockets_set)) 
            {
                memset(buffer, 0, BUFFER_SIZE);
                int read_count = read(sd, buffer, 1024);
                getpeername(sd, (struct sockaddr *) &address, (socklen_t *) &addrlen);
                std::string client_ip = inet_ntoa(address.sin_addr),
                            client_port = std::to_string(ntohs(address.sin_port));
                // Здесь серер отслежиает получение от клиентов уведомлений о завершении их сессии, т.е стоп-команды
                // или отключение клиента в результате непредвиденного завершения их работы.
                // В этих случаях соединение закрывается.

                if (buffer == STOP_COMMAND || read_count == 0)
                {
                    std::cout << "Client disconnected, ip " <<  client_ip << " port "  << client_port  << '\n';
                    close(sd);
                    sd = 0;
                }
                //  После получения сообщения его текст (указвающий директорию для сканирования) передается
                //  на вход утилиты сканирования. Здесь под утилитой имеется в виду - функция, сканирующая директорию.
                //  возращаемое утилитой сообщение с результатом работы отправляется обратно клиенту.
                else
                {
                    std::string scan_result = DirectoryScanner(buffer).inspect_directory();
                    std::cout << "Directory to scan(request from " << client_ip << ":" << client_port
                              << "): " << buffer << '\n';
                    std::cout << "Scan result: " << scan_result << '\n';
                    memset(buffer, 0, BUFFER_SIZE);
                    if (send(sd, scan_result.c_str(), strlen(scan_result.c_str()), 0) < 0)
                        error("ERROR writing to socket");
                }
            }
        }
    }

    // по завершении работы сервер освободит дескриптор своего основного сокета
    close(listen_socket);
}
