//
// Created by artur on 07.07.2021.
//

#ifndef KASPERSKYTEST_UTILITY_H
#define KASPERSKYTEST_UTILITY_H
class DirectoryScanner
{
public:
    std::string inspect_directory();
    DirectoryScanner(const char *path): dir_path {path} {}
private:
    const std::string dir_path;
    int js_count = 0;
    int unix_count = 0;
    int macos_count = 0;
    int errors_count = 0;
    int files_num = 0;

    void inspect_file(const std::string &file_path);
};
#endif //KASPERSKYTEST_UTILITY_H
