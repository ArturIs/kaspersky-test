#include <fstream>
#include <iostream>
#include <filesystem>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <chrono>


int js_count {0}, unix_count {0}, macos_count {0}, errors_count {0}, files_num {0};


void inspect_file(const std::string &file_path)
{
    std::ifstream file(file_path);
    if (!file)
    {
        errors_count++;
        return;
    }

    std::string  str;
    while (file)
    {
        std::getline(file, str);
        if (str == "<script>evil_script()</script>")
        {
            js_count++;
            return;
        }
        if (str == "rm -rf ~/Documents")
        {
            unix_count++;
            return;
        }
        if (str == "system(\"launchctl load /Library/LaunchAgents/com.malware.agent\")")
        {
            macos_count++;
            return;
        }
    }
}


void inspect_directory(const std::string dir_path)
{
    std::queue<std::string> files;
    std::vector<std::thread> threads;
    std::mutex m;
    const int threads_num = std::thread::hardware_concurrency();
    std::cout << "Process will be paralleled on " << threads_num << " cores\n\n";

    for (const auto & file : std::filesystem::directory_iterator(dir_path))
        files.push(file.path());
    std:: cout << "FILES LIST:\n";
    for (int i = 0; i < threads_num; ++i) {
        std::thread t( [&files, &m](int num) {
            while (true)
            {
                m.lock();
                if (!files.empty())
                {
                    std::string file = files.front(), log_string, file_inspect_result;
                    files_num++;
                    files.pop();
                    m.unlock();
                    inspect_file(file);
                } else
                {
                    m.unlock();
                    break;
                }
            }
        }, i);
        threads.push_back(std::move(t));
    }
    for( std::thread& t : threads ) {
        t.join();
    }
}


int main(int argc, char* argv[])
{
    auto begin = std::chrono::steady_clock::now();

    inspect_directory(argv[1]);
    std::cout << "Processed files: " << files_num << '\n'
              << "js count: " << js_count << '\n'
              << "unix count: " << unix_count << '\n'
              << "macos count: " << macos_count << '\n'
              << "errors count: " << errors_count << std::endl;

    auto end = std::chrono::steady_clock::now();
    auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
    std::cout << "Exection time: " << elapsed_ms.count() << " ms\n";
}