//При запуске из консоли можно указать номер порта, на котором запущен сервер на локальной машине

#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <chrono>


const int BUFFER_SIZE = 512;             // размер буфера для обмена текстовыми сообщениями между клиентом и сервером
const std::string STOP_COMMAND = "stop"; // стоп-слово для завершения соединения с клиентом
const char* SERVER_IP = "127.0.0.1";     // работа с сервером, не запущенном на localhost не предусмотрена
const int DEFAULT_SERVER_PORT = 1234;    // по умолчанию этот порт указан для работ с серером


// Вспомогательная функция, выводящая ошибки для откладки
void error(const std::string &error_message)
{
    std::cerr << error_message << '\n';
    exit(EXIT_FAILURE);
}


int main(int argc, char *argv[])
{
    // Можно указать порт, на котором был запущен сервер, если это не порт, определенный по умолчанию
    // посредством аргумента командной строки
    int server_port = DEFAULT_SERVER_PORT;
    if (argc == 2)
    {
        try
        {
            server_port = std::stoi(argv[1]);
        }
        catch (std::invalid_argument)
        {
            error("incorrect port number provided");
        }
        std::cout << "The port provided is " << argv[1] << '\n';
    }


    //******************************************************************************************************************
    // установка соедниния для свзи с сервером. Протокол TCP
    struct sockaddr_in server_addr {0};
    int client_socket;
    char buffer[BUFFER_SIZE];

    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket < 0)
        error("ERROR opening client_socket");

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(server_port);;
    inet_pton(AF_INET, SERVER_IP, &server_addr.sin_addr);
    if (connect(client_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
        error("ERROR connecting to server");


    //******************************************************************************************************************
    // основной цикл для запросов серверу на сканироание директорий и получение результата этих сканирований
    while (true) {
        // ввод из консоли директории для сканирования
        memset(buffer, 0, BUFFER_SIZE);
        std::cout << "Please enter path of the directory to scan: ";
        // засекается время начала работы
        auto begin = std::chrono::steady_clock::now();

        // отправка сообшения серверу. Если была введена стоп команда, клиент завершает работу
        std::cin.getline(buffer, BUFFER_SIZE);
        if (send(client_socket, buffer, strlen(buffer), 0) < 0)
            error("ERROR writing to socket");
        if (buffer == STOP_COMMAND)
            break;

        // получение результата сканирования от серера
        memset(buffer, 0, BUFFER_SIZE);
        int recv_result = recv(client_socket, buffer, BUFFER_SIZE, 0);
        if (recv_result < 0)
            error("ERROR reading from socket");
        if (recv_result == 0)
            error("ERROR server is not available");

        auto end = std::chrono::steady_clock::now();
        auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
        std::cout << buffer;
        std::cout << "Execution time: " << elapsed_ms.count() << " ms" <<  '\n' << std::endl;
    }

    close(client_socket);
}

